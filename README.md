# Java Interface Test Program

School Assignment

This assignment was given to us to test our knowledge of the use of Java Interfaces

**NOTE:** DeliverableTest.java was provided by the instructor of the course.

**Assignment Requirements**
*  Demonstrate OO concepts: interfaces and polymorphism
*  Use a Java INTERFACE class together with concrete classes

**What I learned**
* How to implement Interfaces
* How to apply OO concepts 

**How to run**

Open the project in Eclipse or another Java IDE, and run the "DeliverableTest.java" file.