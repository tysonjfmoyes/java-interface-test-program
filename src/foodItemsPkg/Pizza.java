/*
 * File Name:	Pizza.java
 * Author:		Tyson Moyes
 * Date:		March 31st, 2019
 * Purpose:		Implements Deliverable interface, creates a pizza order with multiple variables for how the pizza should be made. Calculates delivery
 * 				cost using Deliverable interface and sends the result to DeliverableTest.java for output
*/
package foodItemsPkg;
import servicesPkg.*;

/**
 * @author Tyson Moyes
 * @since JDK 1.8_181
 */
public class Pizza implements Deliverable {
	
	// Pizza Class variables
	private int sizeInInches;
	private String typeOfSauce;
	private boolean thinCrust;
	private boolean ham;
	private boolean pepperoni;
	private boolean extraCheese;
	private boolean olives;
	private boolean mushrooms;
	private boolean greenPepper;
	private boolean tomatoes;
	private boolean onions;
	
	// Pizza Class constructors
	
	/**
	 * Construct a Pizza with a specified diameter, sauce type, and crust type
	 * @param sizeInInches - int, size of pizza in inches
	 * @param typeOfSauce - String, type of sauce on pizza
	 * @param thinCrust - boolean, is pizza thin crust? Y/N
	 */
	public Pizza(int sizeInInches, String typeOfSauce, boolean thinCrust) {
		this.sizeInInches = sizeInInches;
		this.typeOfSauce = typeOfSauce;
		this.thinCrust = thinCrust;
	}
	
	/**
	 * Construct a pizza with a specified diameter and sauce type
	 * @param sizeInInches - int, size of pizza in inches
	 * @param typeOfSauce - String, type of sauce on pizza
	 */
	public Pizza (int sizeInInches, String typeOfSauce) {
		this.sizeInInches = sizeInInches;
		this.typeOfSauce = typeOfSauce;
	}
	
	// Pizza Class Methods
	/**
	 * Put extra cheese on this pizza. Set boolean extraCheese to true
	 */
	public void extraCheese() {
		this.extraCheese = true;
	}
	
	/**
	 * Put some green pepper on this pizza. Set boolean greenPepper to true
	 */
	public void addGreenPepper() {
		this.greenPepper = true;
	}
	
	/**
	 * Put some ham on this pizza. Set boolean ham to true
	 */
	public void addHam() {
		this.ham = true;
	}
	
	/**
	 * Put some mushrooms on this pizza. Set boolean mushrooms to true
	 */
	public void addMushrooms() {
		this.mushrooms = true;
	}
	
	/**
	 * Put some olives on this pizza. Set boolean olives to true
	 *
	 * Olives are gross though
	 */
	public void addOlives() {
		this.olives = true;
	}
	
	/**
	 * Put some onion on this pizza. Set boolean onions to true
	 */
	public void addOnion() {
		this.onions = true;
	}
	
	/**
	 * Put some pepperoni on this pizza. Set boolean pepperoni to true
	 */
	public void addPepperoni() {
		this.pepperoni = true;
	}
	
	/**
	 * Put some tomatoes on this pizza. Set boolean tomatoes to true
	 */
	public void addTomatoes() {
		this.tomatoes = true;
	}
	
	/**
	 * get delivery costs for pizza
	 * @return the result of deliveryCosts(), below
	 */
	public double getDeliveryCosts() {return deliveryCosts();}
	
	/**
	 * Calculate delivery costs
	 * @return the cost of delivery for pizza.
	 */
	public double deliveryCosts() {return 5.65;}
}
