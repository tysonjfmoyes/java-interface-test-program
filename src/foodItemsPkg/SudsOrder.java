/*
 * File Name:	SudsOrder.java
 * Author:		Tyson Moyes
 * Date:		March 31st 2019
 * Purpose:		Implements the Deliverable interface. Creates a Suds order with a name, soda id number, and size. Calculates delivery cost using
 * 				Deliverable interface and sends it to DeliverableTest.java for output
*/
package foodItemsPkg;
import servicesPkg.*;

/**
 * @author Tyson Moyes
 * @since JDK 1.8_181
 */
public class SudsOrder implements Deliverable {
	// Class variables
	private String name;
	private int number;
	private int sizeInMl;
	
	// Class constructor
	/**
	 * Create a suds order
	 * @param name - String, the brand of soda
	 * @param number - int, the soda ID number
	 * @param sizeInMl - int, the size of the soda
	 */
	public SudsOrder(String name, int number, int sizeInMl) {
		this.name = name;
		this.number = number;
		this.sizeInMl = sizeInMl;
	}
	
	// Class Methods
	@Override
	public String toString() {
		return "Suds order string";
	}
	
	/**
	 * get delivery costs for suds
	 * @return the result of deliveryCosts(), see below
	 */
	public double getDeliveryCosts() {return deliveryCosts();}
	
	/**
	 * the cost of delivery for suds
	 * @return double, the cost of the delivery
	 */
	public double deliveryCosts() {return 8.48;}
}
