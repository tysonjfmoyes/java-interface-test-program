package deliverableTestPkg;
import foodItemsPkg.*;
import servicesPkg.*;

/**
 * class to create some instances of Deliverable and process them polymorphically
 * @author kriger
 *
 */
public class DeliverableTest {
	final static boolean ThinCrust=true;
	/**
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		Pizza pizza1 = new Pizza(12, "pesto", ThinCrust);
		pizza1.addMushrooms();
		pizza1.addOnion();
		pizza1.addOlives();
		pizza1.addGreenPepper();
		
		Pizza pizza2 = new Pizza(14, "marinara");
		pizza2.addPepperoni();
		pizza2.addMushrooms();
		pizza2.addGreenPepper();
		pizza2.extraCheese();
		
		Deliverable[] deliverableItems = {
				pizza1,
				pizza2,
				new SudsOrder("Barking Squirrel", 6, 473),
				new SudsOrder("Calabogie", 4, 473)
		}; 
		
		int pizzaCount = 0;
		int sudsCount = 0;
		System.out.printf("12345678901234567890123456789\n");
		System.out.printf("%s%21s\n", "Item", "Delivery Cost");
		for (Deliverable d : deliverableItems) {
			if (d instanceof Pizza) {
				System.out.printf("%-12s", "Pizza " + ++pizzaCount);
			}
			else {
				System.out.printf("%-12s", "Suds " + ++sudsCount);
			}
			System.out.printf("$%.2f\n", d.deliveryCosts());
			
		}

	}//main()

}//class
