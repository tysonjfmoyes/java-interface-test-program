/*
 * File Name:	Deliverable.java
 * Author:		Tyson Moyes
 * Date:		March 31st 2019
 * Purpose:		An interface to be implemented by Pizza.java and SudsOrder.java
*/
package servicesPkg;

/**
 * @author Tyson Moyes
 * @since JDK 1.8_181
 */
public interface Deliverable {
	/**
	 * delivery cost
	 * @return the delivery cost as a double
	 */
	double deliveryCosts();
}
